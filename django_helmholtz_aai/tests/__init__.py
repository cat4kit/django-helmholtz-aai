"""Tests for the :mod:`django_helmholtz_aai` app.
"""

# Copyright (C) 2022 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2022-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2
