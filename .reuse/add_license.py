# SPDX-FileCopyrightText: 2022-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Helper script to add licenses to files.

This script can be used to apply the licenses and default copyright holders
to files in the repository.

It uses the short cuts from the ``.reuse/shortcuts.yaml`` file and
adds them to the call of ``reuse annotate``. Any command line option however
overwrites the config in ``shortcuts.yaml``

Usage::

    python .reuse/add_license.py <shortcut> <path> [OPTIONS]
"""

import os.path as osp
from argparse import ArgumentParser
from reuse import header
from reuse.project import create_project
from typing import Dict, TypedDict
from textwrap import dedent

import yaml


class LicenseShortCut(TypedDict):
    """Shortcut to add a copyright statement"""

    #: The copyright statement
    copyright: str

    #: year of copyright statement
    year: str

    #: SPDX Identifier of the license
    license: str


def load_shortcuts() -> Dict[str, LicenseShortCut]:
    """Load the ``shortcuts.yaml`` file."""

    with open(osp.join(osp.dirname(__file__), "shortcuts.yaml")) as f:
        return yaml.safe_load(f)


def add_arguments(
    parser: ArgumentParser, shortcuts: Dict[str, LicenseShortCut]
):
    parser.add_argument(
        "shortcut",
        choices=[key for key in shortcuts if not key.startswith(".")],
        help=(
            "What license should be applied? Shortcuts are loaded from "
            ".reuse/shortcuts.yaml. Possible shortcuts are %(choices)s"
        ),
    )

    header.add_arguments(parser)

    parser.set_defaults(func=header.run)
    parser.set_defaults(parser=parser)


def main(argv=None):
    shortcuts = load_shortcuts()

    parser = ArgumentParser(
        prog=".reuse/add_license.py",
        description=dedent(
            """
            Add copyright and licensing into the header of files with shortcuts

            This script uses the ``reuse annotate`` command to add copyright
            and licensing information into the header the specified files.

            It accepts the same arguments as ``reuse annotate``, plus an
            additional required `shortcuts` argument. The given `shortcut`
            comes from the file at ``.reuse/shortcuts.yaml`` to fill in
            copyright, year and license identifier.

            For further information, please type ``reuse annotate --help``"""
        ),
    )
    add_arguments(parser, shortcuts)

    args = parser.parse_args(argv)

    shortcut = shortcuts[args.shortcut]

    if args.license is None:
        args.license = []
    if args.year is None:
        args.year = []
    if args.copyright is None:
        args.copyright = []

    args.license.append(shortcut["license"])
    args.year.append(shortcut["year"])
    args.copyright.append(shortcut["copyright"])

    project = create_project()
    args.func(args, project)


if __name__ == "__main__":
    main()
