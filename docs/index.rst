.. SPDX-FileCopyrightText: 2022-2023 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0

.. django-helmholtz-aai documentation master file
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to django-helmholtz-aai's documentation!
================================================

|CI|
|Code coverage| |Docs|
|Latest Release|
|PyPI version|
|Code style: black|
|Imports: isort|
|PEP8|
|Checked with mypy|
|REUSE status|

.. rubric:: A generic Django app to login via Helmholtz AAI

Features
--------
Features include

- ready-to-use views for authentification against the Helmholtz AAI
- a new :class:`HelmholtzUser` class based upon djangos
  :class:`~django.contrib.auth.models.User` model and derived from the Helmholtz AAI
- a new :class:`HelmholtzVirtualOrganization` class based upon djangos
  :class:`~django.contrib.auth.models.Group` model and derived from the Helmholtz AAI
- several signals to handle the login of Helmholtz AAI user for your specific
  application
- automated synchronization of VOs of on user authentification

Get started by following the :ref:`installation instructions <installation>`
and have a look into the :ref:`configuration`.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   configuration
   common-problems
   api
   contributing


How to cite this software
-------------------------

.. card:: Please do cite this software!

   .. tab-set::

      .. tab-item:: APA

         .. citation-info::
            :format: apalike

      .. tab-item:: BibTex

         .. citation-info::
            :format: bibtex

      .. tab-item:: RIS

         .. citation-info::
            :format: ris

      .. tab-item:: Endnote

         .. citation-info::
            :format: endnote

      .. tab-item:: CFF

         .. citation-info::
            :format: cff


License information
-------------------
Copyright © 2022-2023 Helmholtz-Zentrum hereon GmbH

The source code of django-helmholtz-aai is licensed under
EUPL-1.2.

If not stated otherwise, the contents of this documentation is licensed under
CC-BY-4.0.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. |CI| image:: https://codebase.helmholtz.cloud/hcdc/django/django-helmholtz-aai/badges/main/pipeline.svg
   :target: https://codebase.helmholtz.cloud/hcdc/django/django-helmholtz-aai/-/pipelines?page=1&scope=all&ref=main
.. |Code coverage| image:: https://codebase.helmholtz.cloud/hcdc/django/django-helmholtz-aai/badges/main/coverage.svg
   :target: https://codebase.helmholtz.cloud/hcdc/django/django-helmholtz-aai/-/graphs/main/charts
.. |Docs| image:: https://readthedocs.org/projects/django-helmholtz-aai/badge/?version=latest
   :target: https://django-helmholtz-aai.readthedocs.io/en/latest/
.. |Latest Release| image:: https://codebase.helmholtz.cloud/hcdc/django/django-helmholtz-aai/-/badges/release.svg
   :target: https://codebase.helmholtz.cloud/hcdc/django/django-helmholtz-aai
.. |PyPI version| image:: https://img.shields.io/pypi/v/django-helmholtz-aai.svg
   :target: https://pypi.python.org/pypi/django-helmholtz-aai/
.. |Code style: black| image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
.. |Imports: isort| image:: https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336
   :target: https://pycqa.github.io/isort/
.. |PEP8| image:: https://img.shields.io/badge/code%20style-pep8-orange.svg
   :target: https://www.python.org/dev/peps/pep-0008/
.. |Checked with mypy| image:: http://www.mypy-lang.org/static/mypy_badge.svg
   :target: http://mypy-lang.org/
.. |REUSE status| image:: https://api.reuse.software/badge/codebase.helmholtz.cloud/hcdc/django/django-helmholtz-aai
   :target: https://api.reuse.software/info/codebase.helmholtz.cloud/hcdc/django/django-helmholtz-aai
