.. SPDX-FileCopyrightText: 2022-2023 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _api:

API Reference
=============

.. toctree::
    :maxdepth: 1

    api/django_helmholtz_aai.app_settings
    api/django_helmholtz_aai.signals
    api/django_helmholtz_aai.urls
    api/django_helmholtz_aai.models
    api/django_helmholtz_aai.views
    Management commands <api/django_helmholtz_aai.management.commands>


.. toctree::
    :hidden:

    api/django_helmholtz_aai
